#pragma once


// CThumbnailDlg 对话框
class CMapeditView;
class CThumbnailDlg : public CDialog
{
	DECLARE_DYNAMIC(CThumbnailDlg)

public:
	CThumbnailDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CThumbnailDlg();

private:
	CMapeditView* m_pView;
	BOOL run_once;
	CDC   m_MemDC;
	CPoint m_RButtonUpPoint;

// 对话框数据
	enum { IDD = IDD_THUMBNAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnJumpToHere();
	afx_msg void OnExportPng();
	afx_msg void OnExportMiniPng();
};
