#if !defined(AFX_ACTLIST_H__E6733791_0432_4E42_BC6C_078BA3B655EC__INCLUDED_)
#define AFX_ACTLIST_H__E6733791_0432_4E42_BC6C_078BA3B655EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CActList dialog
class CScene;
class CEditorDoc;
class CEditorView;

class CActList : public CDialog
{
// Construction
public:
	CEditorView* m_pView;
	CEditorDoc* m_pDoc;
	CScene* m_pData;
	int m_nIndex;	//给鼠标双击和右键菜单用
	CActList(CEditorView* pView,CEditorDoc* pDoc,CScene* pData);   // 构造器
	CActList(CWnd* pParent = NULL);   // standard constructor
	BOOL Create();
	void UpdateList();	//刷新列表显示

// Dialog Data
	//{{AFX_DATA(CActList)
	enum { IDD = IDD_ACTLIST };
	CListCtrl	m_lstActList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CActList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CActList)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnAddone();
	afx_msg void OnDelone();
	afx_msg void OnEditone();
	afx_msg void OnInsert();
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTLIST_H__E6733791_0432_4E42_BC6C_078BA3B655EC__INCLUDED_)
