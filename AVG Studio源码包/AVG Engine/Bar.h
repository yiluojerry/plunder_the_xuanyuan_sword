//Bar.h
//
#if !defined(__BAR_H__)
#define __BAR_H__

class CDB;
class CMidi;

class CBar
{
public:
	CBar();
	~CBar();
	void Reset();
	BOOL InitBar(HWND hwnd,CGameData* pDB,CMidi* pM);
	void Paint(HWND hwnd,HDC hdc);
	int  SetEnemyNum(int n);
	void OnShot();
	BOOL IsFight();		//返回是否可以开始战斗
	BOOL IsRun();		//返回是否还在运行中
//
private:
	void GameOver();	//战斗失败的处理
	CGameData* pDB;		//系统数据堆
	CMidi* pMidi;		//用于播放声音
	HDC hMemDC;			//用于存放角图和按钮的设备
	int nState;			//系统状态0：准备状态；1：允许战斗；2：战斗胜利；3：战斗失败
	BOOL bFire;			//FALSE：未打枪；TRUE：射击中。绘制完成后恢复FALSE
	int nEnemyNum;		//敌人的数量
	int nDelay;			//用于显示“Congratulations !!!”的延时
	BLENDFUNCTION bf;	//用于Alpha混合
	//
};

#endif