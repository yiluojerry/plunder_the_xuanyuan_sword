//fs.cpp

#include "stdafx.h"


//
CFS::CFS()
{
};
//
CFS::~CFS()
{
	DeleteDC( m_hBufDC );
	DeleteDC( m_hEnemyDC );
};
//
BOOL CFS::Init(HWND hwnd,CMidi* pMidi,CGameData* pDB)
{
	HDC hdc = ::GetDC( hwnd );
	m_hBufDC = ::CreateCompatibleDC( hdc );
	HBITMAP hbmp = ::CreateCompatibleBitmap( hdc,640,480 );
	DeleteObject( ::SelectObject(m_hBufDC,hbmp) );
	m_hEnemyDC = ::CreateCompatibleDC( hdc );
	HBITMAP hbmpEnemy = (HBITMAP)LoadImage( NULL,"enemy.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE );
	DeleteObject( ::SelectObject(m_hEnemyDC,hbmpEnemy) );
	::ReleaseDC( hwnd, hdc);
	m_pMidi = pMidi;
	m_pDB	= pDB;
	m_BkBmp.InitBmp( hwnd );
	for( int i=0; i<40; i++ )
	{
		m_Enemy[i].Init( m_hEnemyDC, m_pDB, m_pMidi );
	}
	m_Bar.InitBar( hwnd, pDB, pMidi );
	return TRUE;
};
//
void CFS::OnLButtonDown(int tx,int ty)
{
	if( m_Bar.IsFight() )
	{
		m_Bar.OnShot();
		//调用每一个敌人的挨揍函数判断子弹是否射中敌人
		for( int i=0; i<40; i++ )
		{
			m_Enemy[i].OnShot( tx, ty );
		}
	}
};
//
void CFS::OnTimerPrc(HWND hwnd)
{
	//1、把缓冲涂黑
	BitBlt( m_hBufDC,0,0,640,480,m_hBufDC,0,0,BLACKNESS );
	//2、绘制背景
	m_BkBmp.Paint( hwnd, m_hBufDC );
	//3、绘制所有的敌人
	int i;
	for( i=0; i<40; i++ )
	{
		m_Enemy[i].Paint( hwnd, m_hBufDC );
	}
	//4、绘制前景信息
	m_Bar.Paint( hwnd, m_hBufDC );
	//5、显示主缓冲
	HDC hdc = ::GetDC( hwnd );
	BitBlt( hdc,0,0,640,480,m_hBufDC,0,0,SRCCOPY );
	::ReleaseDC( hwnd, hdc);
	//6、根据前景信息条的状态判断是否可以结束本段落
	int nEnemyNum = 0;	//统计还有多少活着的敌人
	for( i=0; i<40; i++ )
	{
		if( m_Enemy[i].IsLiving() )
		{
			nEnemyNum ++;
		}
	}
	m_Bar.SetEnemyNum( nEnemyNum );
	if( m_Bar.IsRun() )
		return;
	//7、段落结束（即：消灭了全部敌人）执行全部运算序列。
	if( !CluAll(m_pDB) )
	{
		KillTimer(hwnd,1000);
		MessageBox(hwnd,"运算指令错误！请检查战斗场脚本文件运算段的正确性。",NULL,MB_ICONERROR|MB_OK);
		::PostQuitMessage( WM_QUIT );
	}
	//8、修改当前系统，表示战斗场已经执行完
	m_pDB->SetCurSystem( 0 );
};//OnTimerPrc()函数结束
//
BOOL CFS::Open(HWND hwnd,char* FileName)
{
	if( !CScene::Open(FileName) )
		return FALSE;
	//文件打开后，立即执行全部配置序列
	ACT act;
	while( GetNextAct(&act) )
	{
		if( !OnCode(hwnd,&act) )
		{
			KillTimer(hwnd,1000);
			MessageBox(hwnd,"动作指令错误！请检查战斗场脚本文件动作段的正确性。",NULL,MB_ICONERROR|MB_OK);
			::PostQuitMessage( WM_QUIT );
		}
	}
	m_Bar.Reset();
	return TRUE;
};//Open()函数结束
//
BOOL CFS::OnCode(HWND hwnd,ACT* pAct)
{
	int n = pAct->nActType;
	if( n<1 || n>10 || (n>4 && n<8) )
		return FALSE;
	char fname[255];
	switch( n )
	{
	case 1:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".bmp" );
		m_BkBmp.Open( fname,0,0,640,480 );
		break;
	case 2:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".mid" );
		m_pMidi->Play( hwnd, fname );
		break;
	case 3:
		m_pMidi->Stop();
		break;
	case 4:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".wav" );
		m_pMidi->PlayWav( fname );
		break;
	case 9:
		m_Enemy[pAct->nPara1].Set( (pAct->nPara2),(pAct->nPara3),(pAct->nPara4),(pAct->nPara5) );
		break;
	}
	return TRUE;
};//OnCode()函数结束
//the end.